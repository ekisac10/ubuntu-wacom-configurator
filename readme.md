# Ubuntu Wacom Configurator

![banner](figures/banner.png)

This script is designed to help configure the [Wacom](https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.powerplanetonline.com%2Fcdnassets%2Ftableta_digitalizadora_wacom_intuos_comfort_bt_tamano_m_pistacho_01_l.jpg&imgrefurl=https%3A%2F%2Fwww.powerplanetonline.com%2Fen%2Fwacom-intuos-comfort-bt-digitizer-tablet-size-m-pistachio&tbnid=aRI8oGG5u-iO4M&vet=12ahUKEwjZxrDO2cj1AhVFXcAKHcgdBv8QMygCegUIARDrAg..i&docid=AiNV02v8aOJV7M&w=800&h=800&itg=1&q=wacom%20tablet&ved=2ahUKEwjZxrDO2cj1AhVFXcAKHcgdBv8QMygCegUIARDrAg) Tablet
in a Ubuntu Linux environment. 

I wrote this script as I faced challenges setting the Wacom tablet to work specifically on the screen I was using 
for presentations as opposed to the tablet working on all screens. When the tablet area is scaled to multiple screens, the write
area is greatly reduced.

Assuming you have 2 or more screens, this script will detect all available screens and prompt the user to select the 
specific screen from which they want to use the tablet.

The image below shows how the script works

![overview](figures/overview.png)

## Installation

This script has been test on Ubuntu 20.04 LTS. It's designed specifically for Linux environments
though it may not work on all Linux distros. Feel free to modify the source code to suit other distros.

~~~~bash
git clone

cd wacom_configurator 
chmod +x wacom_configurator.py
pip3 install -r requirements.txt
./wacom_configurator.py
~~~~

## License
Unless otherwise specified, everything in this repository is covered by the following licence:

- *Wacom Configurator* is licensed under the [European Union Public License v1.2 (EUPLv1.2)](https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf).

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

<br><br>
******
Version 1.0, Jan 2022

© Conrad Ekisa
******
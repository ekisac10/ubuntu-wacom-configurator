#!/usr/bin/python3

import os
import subprocess
from termcolor import colored
import distro
import sys

__author__ = "Conrad Ekisa"
__copyright__ = "Conrad Ekisa"
__licence__ = "European Union Public Licence v1.2"
__version__ = "1.0"
__program__ = "Ubuntu Wacom Configurator"


def check_tool_exists(tool: str):
    if tool == "xsetwacom":
        # https://command-not-found.com/xsetwacom
        install = "apt install xserver-xorg-input-wacom"
        rc = subprocess.call(['which', "{}".format(tool)], stdout=subprocess.PIPE)
        if rc == 0:
            return True
        else:
            print(colored("{} is not installed. Install it with the command {}".format(tool, install), "red"))
            return False
    if tool == "xrandr":
        # https://command-not-found.com/xrandr
        install = "apt install x11-xserver-utils"
        rc = subprocess.call(['which', "{}".format(tool)], stdout=subprocess.PIPE)
        if rc == 0:
            return True
        else:
            print(colored("{} is not installed. Install it with the command {}".format(tool, install), "red"))
            return False
    else:
        rc = subprocess.call(['which', "{}".format(tool)], stdout=subprocess.PIPE)
        if rc == 0:
            return True
        else:
            print(colored("{} is not installed.".format(tool), "red"))
            return False


def detect_stylus():
    result = os.popen("xsetwacom --list devices | grep stylus | grep -oP '(?<=id:).*(?=type)'").read()
    if result == "":
        print(colored("\nWacom Tablet not Detected. Connect your tablet and try again.", "red"))
        exit()
    else:
        return result.strip("\n").strip("\t")


def select_monitor():
    print(colored("Detecting Monitors ...\n", "magenta"))
    result = os.popen("xrandr --listactivemonitors").read().split("\n")
    print(colored(result[0], "cyan"))
    count = 1
    monitor_dict = {}
    for monitor in result:
        try:
            monitor = monitor.split("  ")[1]
            print("{}: {}".format(count, monitor))
            monitor_dict[str(count)] = monitor
            count += 1
        except Exception as e:
            pass
    monitor_select = input(colored("\nSelect the Monitor you wish to use the Wacom Tablet on [number]: ", "magenta"))
    while monitor_select not in monitor_dict.keys():
        print(colored("Invalid number selection. Please try again.", "red"))
        monitor_select = input(colored("\nSelect the Monitor you wish to use the Wacom Tablet on [number]: ", "magenta"))
    print(colored("Monitor: {}({}) has been selected. Configuring now ...".format(monitor_select, monitor_dict[monitor_select]), "green"))
    return monitor_dict[monitor_select]


def configure_wacom(stylus_id: str, monitor: str):
    try:
        os.popen("xsetwacom --set {} MapToOutput {}".format(stylus_id, monitor))
        print(colored("\nConfiguration Complete. Exiting ...", "green"))
    except Exception as e:
        print(colored("There was an error:", "red"), colored(e, "red", attrs=["bold"]))
        print(colored("Exiting ...", "red"))
        exit()


def print_author_info(info: dict):
    key_length = 0
    for item in info.keys():
        if len(item) > key_length:
            key_length = len(item)
    for key, value in info.items():
        print(colored(key, "red", attrs=["bold"]) + " "*(key_length-len(key)) + "  :  " + colored(value, "green", attrs = ["bold"]))
    print("")


header = """
 __          __                          _____             __ _                       _             
 \ \        / /                         / ____|           / _(_)                     | |            
  \ \  /\  / /_ _  ___ ___  _ __ ___   | |     ___  _ __ | |_ _  __ _ _   _ _ __ __ _| |_ ___  _ __ 
   \ \/  \/ / _` |/ __/ _ \| '_ ` _ \  | |    / _ \| '_ \|  _| |/ _` | | | | '__/ _` | __/ _ \| '__|
    \  /\  / (_| | (_| (_) | | | | | | | |___| (_) | | | | | | | (_| | |_| | | | (_| | || (_) | |   
     \/  \/ \__,_|\___\___/|_| |_| |_|  \_____\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__\___/|_|   
                                                                 __/ |                              
                                                                |___/                               
"""

author_info = {"Build by": "Conrad Ekisa", "Version": "v1.0", "Intended Platform": "Linux (Ubuntu)"}

print(colored(header, "green"))
print_author_info(author_info)

if sys.platform != "linux":
    print(colored("ERROR: This script is designed to work on Linux Environments", "red"))
    exit()
if distro.name() != "Ubuntu":
    state = input(colored("Your Linux distro is: {}. This script has only been tested on Ubuntu and"
                  " may not work in your environment. Do you wish to continue [Y/N]? >> ".format(distro.name()), "green"))
    if state == "N" or state == "n":
        print(colored("Gracefully exiting ...", "green"))
        exit()
    elif state == "Y" or state == "y":
        pass
    else:
        print(colored("Invalid Key pressed. Exiting ...", "red"))


xsetwacom_state = check_tool_exists("xsetwacom")
xrandr_state = check_tool_exists("xrandr")
if xsetwacom_state == False or xrandr_state == False:
    print(colored("\nInstall the required packages and retry the script", "green"))
    exit()

stylus_id = detect_stylus()
selected_monitor = select_monitor()
configure_wacom(stylus_id, selected_monitor)
